from __future__ import unicode_literals

import errno
import functools
import mock
import os
import pathlib
import shutil
import subprocess
import tempfile
import unittest

from six import BytesIO

import zc.buildout

import rubygems


def touch(path):
    with path.open('w') as f:
        f.write('')


class fixture(object):
    def __init__(self, options=None, version='1.0'):
        self.options = options or {}
        self.version = version

    def __call__(self, func):
        @functools.wraps(func)
        def wrapper(test):
            buildout, name, options = self.set_up()
            cwd = os.getcwd()
            os.chdir(str(self.tempdir))
            func(test, self.tempdir, self.patches, buildout, name, options)
            os.chdir(cwd)
            self.tear_down()
        return wrapper

    def patch(self, modules):
        self.patchers = {}
        self.patches = {}
        for name, module in modules:
            self.patchers[name] = mock.patch(module)
            self.patches[name] = self.patchers[name].start()

    def makedirs(self, dirs):
        self.tempdir = pathlib.Path(tempfile.mkdtemp())
        for directory in dirs:
            os.makedirs(str(self.tempdir / directory))

    def set_up(self):
        name = 'rubygems'
        version = self.options.get('return', {}).get('version', self.version)

        self.patch((
            ('check_output', 'rubygems.subprocess.check_output'),
            ('urlopen', 'rubygems.urllib.request.urlopen'),
            ('hexagonit', 'rubygems.hexagonit.recipe.download.Recipe'),
        ))
        self.patches['urlopen'].return_value = BytesIO(
            b'https://rubygems.org/rubygems/rubygems-1.0.zip'
        )

        self.makedirs((
            'bin',
            'ruby-%s' % version,
            'rubygems-%s' % version,
            'rubygems/bin',
        ))

        buildout = {'buildout': dict({
            'parts-directory': str(self.tempdir),
            'bin-directory': str(self.tempdir / 'bin'),
        }, **self.options.get('buildout', {}))}

        options = self.options.get('recipe', {})

        return buildout, name, options

    def tear_down(self):
        for patcher in self.patchers.values():
            patcher.stop()
        shutil.rmtree(str(self.tempdir))


class RubyGemsTests(unittest.TestCase):
    @fixture({'recipe': {'gems': 'sass'}})
    def test_success(self, path, patches, buildout, name, options):
        recipe = rubygems.Recipe(buildout, name, options)
        recipe.install()

        # One urlopen call to get latest version
        self.assertEqual(patches['urlopen'].call_count, 1)

        args = patches['urlopen'].mock_calls[0][1]
        self.assertEqual(args, ('https://rubygems.org/pages/download',))

        # One hexagonit.recipe.download call to download rubygems
        self.assertEqual(patches['hexagonit'].call_count, 1)

        args = patches['hexagonit'].mock_calls[0][1]
        self.assertEqual(args[2], {
            'url': (
                'https://rubygems.org/rubygems/rubygems-1.0.zip'
            ),
            'destination': str(path),
        })

        # Two check_output calls to install rubygems and specified gem
        self.assertEqual(patches['check_output'].call_count, 2)

        args = patches['check_output'].mock_calls[0][1]
        self.assertEqual(args[0], [
            'ruby', 'setup.rb', 'all', '--prefix=%s/rubygems' % path,
            '--no-rdoc', '--no-ri',
        ])

        args = patches['check_output'].mock_calls[1][1]
        self.assertEqual(args[0], [
            None, 'install', '--no-rdoc', '--no-ri',
            '--bindir=%s/rubygems/bin' % path,
            'sass', '--'
        ])

    @fixture({'recipe': {}})
    def test_missing_gems(self, path, patches, buildout, name, options):
        self.assertRaises(
            zc.buildout.UserError,
            rubygems.Recipe, buildout, name, options
        )

    @fixture({'recipe': {'gems': 'sass'}})
    def test_oserror(self, path, patches, buildout, name, options):
        patches['check_output'].side_effect = OSError
        recipe = rubygems.Recipe(buildout, name, options)
        self.assertRaises(zc.buildout.UserError, recipe.install)

    @fixture({'recipe': {'gems': 'sass'}})
    def test_signal_received(self, path, patches, buildout, name, options):
        exception = subprocess.CalledProcessError(-1, '')
        patches['check_output'].side_effect = exception
        recipe = rubygems.Recipe(buildout, name, options)
        self.assertRaises(zc.buildout.UserError, recipe.install)

    @fixture({'recipe': {'gems': 'sass'}})
    def test_non_zero_exitcode(self, path, patches, buildout, name, options):
        exception = subprocess.CalledProcessError(1, '')
        patches['check_output'].side_effect = exception
        recipe = rubygems.Recipe(buildout, name, options)
        self.assertRaises(zc.buildout.UserError, recipe.install)

    @fixture({'recipe': {'gems': 'sass'}})
    def test_update(self, path, patches, buildout, name, options):
        recipe = rubygems.Recipe(buildout, name, options)
        recipe.update()

    @fixture({'recipe': {'gems': 'sass', 'environment': 'invalid'}})
    def test_invalid_environment(self, path, patches, buildout, name, options):
        recipe = rubygems.Recipe(buildout, name, options)
        self.assertRaises(zc.buildout.UserError, recipe.install)

    @fixture({'recipe': {
        'gems': 'sass',
        'url': 'https://rubygems.org/rubygems/rubygems-1.0.zip',
    }})
    def test_version_from_url(self, path, patches, buildout, name, options):
        recipe = rubygems.Recipe(buildout, name, options)
        recipe.install()

    @fixture({'recipe': {'gems': 'sass', 'version': '1.0'}})
    def test_version(self, path, patches, buildout, name, options):
        recipe = rubygems.Recipe(buildout, name, options)
        recipe.install()

    @fixture({'recipe': {'gems': 'sass'}})
    def test_no_version(self, path, patches, buildout, name, options):
        patches['urlopen'].return_value = BytesIO(b'')
        recipe = rubygems.Recipe(buildout, name, options)
        self.assertRaises(zc.buildout.UserError, recipe.install)

    @fixture({'recipe': {'gems': 'sass'}})
    @mock.patch('rubygems.os.mkdir')
    def test_mkdir_error(self, path, patches, buildout, name, options, mkdir):
        mkdir.side_effect = OSError(errno.EIO)
        recipe = rubygems.Recipe(buildout, name, options)
        self.assertRaises(zc.buildout.UserError, recipe.install)

    @fixture({'recipe': {'gems': 'sass'}})
    def test_executables(self, path, patches, buildout, name, options):
        recipe = rubygems.Recipe(buildout, name, options)
        touch(pathlib.Path(recipe.options['location']) / 'bin/sass')
        recipe.install()

    @fixture({'recipe': {'gems': 'sass==1.0'}})
    def test_pinned_versions(self, path, patches, buildout, name, options):
        recipe = rubygems.Recipe(buildout, name, options)
        touch(path / 'rubygems/bin/gem')
        recipe.install()

        args = patches['check_output'].mock_calls[0][1]
        self.assertEqual(args[0], [
            '%s/rubygems/bin/gem' % path, 'install', '--no-rdoc', '--no-ri',
            '--bindir=%s/rubygems/bin' % path,
            'sass', '--version=1.0', '--'
        ])
